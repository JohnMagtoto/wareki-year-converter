# Gregorian Calendar to Japanese Year (Wareki) Converter

## NOTE

Currently in beta

## Installation

```shell
npm install wareki-year-converter
```

### Usage

Usage

```ts

import { convertToWarekiYear } from 'wareki-year-converter';

...

convertToWarekiYear('2018'); // returns `平成30年`
convertToWarekiYear(moment('2017-01-01'));// returns `平成29年`

```

Alternate usage

```ts
import { WarekiYearConverter } from 'wareki-year-converter';
...
const warekiYearConverter = new WarekiYearConverter();

warekiYearConverter.convertToWarekiYear('2018');
...
warekiYearConverter.convertToWarekiYear(moment('2018-01-01'));
```
