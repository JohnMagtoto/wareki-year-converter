'use strict';
var expect = require('chai').expect;
var index = require('../dist/index.js');
var moment = require('moment');

describe('WarekiYearConverter', () => {
	let warekiYC;

	beforeEach(() => {});

	it('should return 平成29年', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			'2017'
		);
		expect(result).to.equal('平成29年');
	});

	it('should return 平成30年', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			'2018'
		);
		expect(result).to.equal('平成30年');
	});

	it('should return 平成31年/令和1年', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			'2019'
		);
		expect(result).to.equal('平成31年/令和1年');
	});

	it('should return 令和2年', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			'2020'
		);
		expect(result).to.equal('令和2年');
	});

	it('should return 令和10年', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			'2028'
		);
		expect(result).to.equal('令和10年');
	});

	it('should return 明治27年', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			'1894'
		);
		expect(result).to.equal('明治27年');
	});

	it('should return Improper year value error: asdf', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			'asdf'
		);
		expect(result).to.equal(null);
	});

	it('should return Improper year value error: 2018-05-09', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			'2018-05-09'
		);
		expect(result).to.equal(null);
	});

	it('should return Improper year value error: 12345', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			'12345'
		);
		expect(result).to.equal(null);
	});

	it('should be able to process moment object year "2017"', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			moment('2017-01-01', 'YYYY-MM-DD')
		);
		expect(result).to.equal('平成29年');
	});

	it('should be able to process until the first year of the Meiji era', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			'1868'
		);
		expect(result).to.equal('明治1年');
	});

	it('should not be able to process years earlier than Meiji era', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			moment('1860-01-01', 'YYYY-MM-DD')
		);
		expect(result).to.equal(null);
	});

	it('should process years in type of number', () => {
		const result = new index.WarekiYearConverter().convertToWarekiYear(
			2017
		);
		expect(result).to.equal('平成29年');
	});

	it('could proces years using the convenience function: `convertToWarekiYear`', () => {
		const result = index.convertToWarekiYear(2019);
		expect(result).to.equal('平成31年/令和1年');
	});
});
